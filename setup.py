from setuptools import find_packages, setup

with open("relative_reader/_version.py") as f:
    version = f.readlines()[-1].split()[-1].strip("\"'")

requirements = ["numpy", "scipy", "py3nj"]

info = {
    "name": "relative_reader",
    "version": version,
    "maintainer": "Soham Pal",
    "maintainer_email": "dssohampal@gmail.com",
    "url": "https://codeberg.org/e-eight/relative-reader",
    "license": "MIT",
    "packages": find_packages(where="."),
    "entry_points": {},
    "description": "Reads in (reduced) matrix elements in a relative basis, from file.",
    "provides": ["relative_reader"],
    "install_requires": requirements,
    "package_data": {},
    "include_package_data": True,
}

classifiers = [
    "Development Status :: Alpha",
    "Environment :: Console",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: Apache Software License",
    "Natural Language :: English",
    "Operating System :: POSIX",
    "Operating System :: MacOS :: MacOS X",
    "Operating System :: POSIX :: Linux",
    "Operating System :: Microsoft :: Windows",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3 :: Only",
    "Topic :: Scientific/Engineering :: Physics",
]

setup(classifiers=classifiers, **(info))

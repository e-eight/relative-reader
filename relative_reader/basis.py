from collections import namedtuple

BasisState = namedtuple("BasisState", ["n", "l", "s", "j", "mj", "t", "mt"])
ReducedBasisState = namedtuple("ReducedBasisState", ["n", "l", "s", "j", "t"])


def basis_state_from_reduced(state: ReducedBasisState, mj: int, mt: int):
    return BasisState(state.n, state.l, state.s, state.j, mj, state.t, mt)


def antisymmetry_rule(l: int, s: int, t: int):
    return (l + s + t) % 2 != 0


def triangle_rule(l: int, s: int, j: int):
    return abs(l - s) <= j <= l + s


def create_basis(
    Nmax: int, *, l: int, s: int, j: int, is_coupled: bool, mj: int, t: int, mt: int
):
    """Create a relative (coupled) LS basis."""

    if t not in (0, 1):
        raise ValueError("Total isospin `t` can only be 0 or 1.")
    if mt not in (-1, 0, 1):
        raise ValueError("Isospin projection `mt` can only be -1, 0, or 1.")
    if abs(mt) > t:
        raise ValueError("`|mt|` must be less than or equal to `t`.")
    if not antisymmetry_rule(l, s, t):
        raise ValueError("The entered quantum numbers violate antisymmetry.")
    if abs(mj) > j:
        raise ValueError("`|mj|` cannot be larger than total spin `j`.")

    # Get all the orbital angular momentum quantum numbers.
    orbital_quantum_numbers = [l]
    if is_coupled:
        for ll in range(abs(j - s), min(j + s, Nmax) + 1):
            if ll != l and antisymmetry_rule(ll, s, t) and triangle_rule(ll, s, j):
                orbital_quantum_numbers.append(ll)
    orbital_quantum_numbers.sort()

    # Generate basis.
    states = []
    for n in range(min(orbital_quantum_numbers), Nmax + 1, 2):
        for ll in orbital_quantum_numbers:
            if ll <= n:
                states.append(BasisState(n, ll, s, j, mj, t, mt))

    return states

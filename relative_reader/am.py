import numpy as np
import py3nj


def hat(j: int) -> float:
    return np.sqrt(2 * j + 1)


def clebsch_gordan(j1: int, j2: int, j3: int, mj1: int, mj2: int, mj3: int):
    return py3nj.clebsch_gordan(2 * j1, 2 * j2, 2 * j3, 2 * mj1, 2 * mj2, 2 * mj3)

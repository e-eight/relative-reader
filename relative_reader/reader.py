from collections import namedtuple
from itertools import islice, product
from pathlib import Path
from typing import Dict, List, Union

import numpy as np
from scipy.sparse import lil_matrix

from relative_reader.am import clebsch_gordan, hat
from relative_reader.basis import (
    BasisState,
    ReducedBasisState,
    basis_state_from_reduced,
)

Header = namedtuple(
    "Header", ["J0", "g0", "T0_min", "T0_max", "symmetry_phase_mode", "Nmax", "Jmax"]
)
BraKet = namedtuple("BraKet", ["fstate", "istate"])


class NDRelData:
    def __init__(
        self,
        ndrel_file: Union[Path, str],
        T0: int,
        ibasis: List[BasisState],
        fbasis: List[BasisState],
    ):
        if not Path(ndrel_file).exists():
            raise ValueError("File does not exist.")
        self._path = Path(ndrel_file)

        self._header = self._read_header()

        if not self._header.T0_min <= T0 <= self._header.T0_max:
            raise ValueError(f"File is not defined for T0 = {T0}.")
        self._T0 = T0

        if not ibasis:
            raise ValueError("Initial basis must be provided.")
        self._ibasis = ibasis
        self._mji = ibasis[0].mj
        self._mti = ibasis[0].mt

        if not fbasis:
            raise ValueError("Final basis must be provided.")
        self._fbasis = fbasis
        self._mjf = fbasis[0].mj
        self._mtf = fbasis[0].mt

        self._data = {}

    def _read_header(self) -> Header:
        header = []
        with self._path.open() as f:
            for line in islice(f, 6, 8):
                header.extend([int(x) for x in line.split()])
        return Header(*header)

    def print_info(self):
        print("Operator details")
        print("----------------")
        print(f"Operator spin: {self._header.J0}, Operator parity: {self._header.g0}")
        print(
            f"Operator isospin range: {self._header.T0_min} to {self._header.T0_max}\n"
        )
        print("Basis details")
        print("-------------")
        print(f"Basis Nmax: {self._header.Nmax}, Basis Jmax: {self._header.Jmax}")

    def todict(self) -> Dict[BraKet, float]:
        def read_line(line):
            line_elements = line.split()
            T0 = int(line_elements[0])
            fstate = ReducedBasisState(*[int(x) for x in line_elements[1:6]])
            istate = ReducedBasisState(*[int(x) for x in line_elements[6:11]])
            key = BraKet(
                basis_state_from_reduced(fstate, self._mjf, self._mtf),
                basis_state_from_reduced(istate, self._mji, self._mti),
            )
            rme = float(line_elements[-1])
            return T0, key, rme

        count = 0
        with self._path.open() as f:
            for line in f:
                count += 1
                if count > 8:
                    T0, key, rme = read_line(line)
                    if T0 == self._T0:
                        self._data[key] = rme
        return self._data

    def tomatrix(self, dtype=np.float32, return_type: str = "sparse"):
        if not self._data:
            self.todict()

        matrix = lil_matrix((len(self._ibasis), len(self._fbasis)), dtype=dtype)
        for (i, istate), (j, fstate) in product(
            enumerate(self._ibasis), enumerate(self._fbasis)
        ):
            if BraKet(istate, fstate) in self._data:
                symmetry_factor = 1
                element = self._data[BraKet(istate, fstate)]
            elif BraKet(fstate, istate) in self._data:
                symmetry_factor = (
                    (-1) ** (istate.j + istate.t - fstate.j - fstate.t)
                    * (hat(istate.j) / hat(fstate.j))
                    * (hat(istate.t) / hat(fstate.t))
                )
                element = self._data[BraKet(fstate, istate)]
            else:
                continue

            spin_cg = clebsch_gordan(
                istate.j,
                self._header.J0,
                fstate.j,
                istate.mj,
                fstate.mj - istate.mj,
                fstate.mj,
            )

            isospin_cg = clebsch_gordan(
                istate.t,
                self._T0,
                fstate.t,
                istate.mt,
                fstate.mt - istate.mt,
                fstate.mt,
            )

            matrix[i, j] = symmetry_factor * spin_cg * isospin_cg * element

        return matrix if return_type == "sparse" else matrix.toarray()

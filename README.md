# relative-reader

Reads in (reduced) matrix elements in a relative basis from file, and writes them to a matrix.